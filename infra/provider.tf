# GCP Proivder + a local state b/c I don't feel like creating a GCS bucket to store it. :LOL:
# Since applying locally, my creds are located in env called GOOGLE_CREDENTIALS. 

provider "google" {
    project = "GKE-Automation"
    region  = "us-central1"
}

provider "kubernetes" {
    config_path = "~/.kube/config"
    config_context = "hyland-demo-context"
}

provider "helm" {
    kubernetes {
        config_path = "~/.kube/config"
    }
}

