variable "istio-repo" {
    type = string
    default = "https://istio-release.storage.googleapis.com/charts"
}

variable "httpbin-repo" {
    type = string
    default = "https://matheusfm.dev/charts"
}
