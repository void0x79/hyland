# Istio base + ingress deployments. 
# Deployment via helm chart and use the latest version of the chart.

resource "helm_release" "istio-base" {
    depends_on = [ kubernetes_namespace.istio-namespace ] 
    name       = "istio-base"
    repository = var.istio-repo
    chart      = "istio/base"
    namespace  = "istio-base"
}

resource "helm_release" "istio-ingress" {
    depends_on  = [ kubernetes_namespace.istio-ingress ]
    name        = "istio-ingress"
    repository  = var.istio-repo
    chart       = "istio/gateway"
    namespace   = "istio-ingress"
}

resource "helm_release" "httpbin" {
    depends_on = [ helm_release.istio-base, helm_release.istio-ingress ]
    name       = "httpbin"
    repository = var.httpbin-repo
    chart      = "matheusfm/httpbin"
    namespace  = "httpbin"
}
