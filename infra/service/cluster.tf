# How the actual cluster layout looks post cluster provisioning.
# Uses depends on to have proper plan/apply ordering.

# NAMESPACE
resource "kubernetes_namespace" "istio-namespace" {
    metadata {
        name = "istio-base"
    }
}

resource "kubernetes_namespace" "istio-ingress" {
    metadata {
        name = "istio-ingress"
    }

    # This label will automagically do the sidecar injection via a mutating webhook.
    labels = {
        isito-injection = "enabled"
    }
}

resource "kubernetes_namespace" "httpbin" {
    metadata {
        name = "httpbin"
    }

    labels = {
        istio-injection = "enabled"
    }
}
