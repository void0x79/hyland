# Pretty minimal setup, not using a module b/c I don't feel like it :) 

resource "google_container_cluster" "hyland-cluster" {
    name     = "hyland-cluster"
    location = "us-central1"
    remove_default_node_pool = true
    enable_shielded_nodes = true
    initial_node_count = 1 # <------ will use another managed node pool, but rules are rules.

    # Autoscale this thang. # Use default resouce_limits b/c this is a demo :) 
    cluster_autoscaling {
        enabled = true
    }

    # Master_authorized_networks
    master_authorized_networks_config {
        display_name = "hyland-demo-block"
        cidr_block   = "8.8.8.8/32"
    }
}

resource "google_container_node_pool" "managed-node-pool" {
    name                  = "hyland-managed-np"
    location              = "us-central1"
    cluster               = google_container_cluster.hyland-cluster.name
    node_count            = 2 # <---- 2 b/c I ain't made of money :D

    node_config {
        preemptible = true
        machine_type = "e2-small"
    }
}
